import UIKit
import EventKit
class SecondViewController: UIViewController, UIPickerViewDelegate, UIToolbarDelegate, UITextFieldDelegate {
    var txtActiveField = UITextField()
    var toolBar: UIToolbar!
    
    let pickerView = UIPickerView()
    let myDatePicker1: UIDatePicker = UIDatePicker()
    let myDatePicker2: UIDatePicker = UIDatePicker()
    
    @IBOutlet weak var workSecLabel: UILabel!
    @IBOutlet weak var workTimesLabel: UILabel!

    @IBOutlet weak var event: UITextField!
    @IBOutlet weak var startTime: UITextField!
    @IBOutlet weak var endTime: UITextField!
    @IBOutlet weak var memo: UITextField!
    
    var myStartDate: NSDate!
    var myEndDate: NSDate!
    
    var myPlan: String!
    var myMemo: String!
    var myEventStore: EKEventStore!
    
    @IBOutlet weak var icloud: UIButton!
    @IBOutlet weak var oneTouch: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        icloud.layer.cornerRadius = 10
        oneTouch.layer.cornerRadius = 5
        myEventStore = EKEventStore()
        allowAuthorization()
        self.title = "詳細設定"
        startTime.placeholder = "00時00分"
        endTime.placeholder = "00時00分"
        
        // To Be Reviewed
        let defaults = NSUserDefaults.standardUserDefaults()
        let myTaskData = defaults.integerForKey("taskData")
        let mySec = defaults.integerForKey("sec")
        
        workTimesLabel.text = NSString(format: "%02d", myTaskData) as String
        workSecLabel.text = NSString(format: "%02d", mySec) as String
        
        event.delegate = self
        memo.delegate = self
        
        // To Be Reviewed
        pickerView.showsSelectionIndicator = true
        
        // datePickerを設定1（デフォルト位置は画面上部）
        myDatePicker1.frame = CGRectMake(0, self.view.bounds.height/4, 0, 0)
        myDatePicker1.timeZone = NSTimeZone.localTimeZone()
        myDatePicker1.backgroundColor = UIColor.whiteColor()
        myDatePicker1.layer.cornerRadius = 5.0
        myDatePicker1.layer.shadowOpacity = 0.5
        
        // datePickerを設定2（デフォルト位置は画面上部）
        myDatePicker2.frame = CGRectMake(0, self.view.bounds.height/4, 0, 0)
        myDatePicker2.timeZone = NSTimeZone.localTimeZone()
        myDatePicker2.backgroundColor = UIColor.whiteColor()
        myDatePicker2.layer.cornerRadius = 5.0
        myDatePicker2.layer.shadowOpacity = 0.5
        
        // 値が変わった際のイベントを登録する.
        myDatePicker1.addTarget(self, action: "onDidChangeDate1:", forControlEvents: .ValueChanged)
        myDatePicker2.addTarget(self, action: "onDidChangeDate2:", forControlEvents: .ValueChanged)
        
        // ■■■UIToolbar（pickerDate上にある。）
            // UIToolbar
            toolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
            toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
            toolBar.barStyle = .BlackTranslucent
            toolBar.tintColor = UIColor.whiteColor()
            toolBar.backgroundColor = UIColor.blackColor()
            
            // UIToolbarBtn
            let toolBarBtn = UIBarButtonItem(title: "完了", style: .Plain, target: self, action: "tappedToolBarBtn:")
            toolBarBtn.tag = 0
            toolBar.items = [toolBarBtn]
        
        // キーボードの代わりにDatePickerを表示
        startTime.inputView = myDatePicker1
        endTime.inputView = myDatePicker2
        
        // To Be Reviewed
        startTime.inputAccessoryView = toolBar
        endTime.inputAccessoryView = toolBar
        
        // ■■■UINavigationControllerのボタン
            // NavigationBarを取得する.
            self.navigationController?.navigationBar
            
            // NavigationBarを表示する.
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        
            // BarButtonItemを取得する.
            self.navigationItem

            // BarButtonItemを作成する.
            let myBarButton = UIBarButtonItem(title: "説明", style: .Plain, target: self, action: "onClickMyBarButton:")
            
            // Barの右側に配置する.
            self.navigationItem.setRightBarButtonItem(myBarButton, animated: true)
        
        addSwipeRecognizer()
        }
        // viewDidLoad、ここで終わり
    
    // To Be Reviewed
    // スワイプ検知用に登録
    func addSwipeRecognizer() {
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        
        var swipeUp = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeUp)
        self.view.addGestureRecognizer(swipeDown)
    }
    
    // スワイプ処理
    func swiped(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Left:
                // 左
                println("left")
                // To Be Reviewed
                // icloud(<#sender: UIButton#>)
            case UISwipeGestureRecognizerDirection.Right:
                // 右
                println("right")
                self.navigationController?.popViewControllerAnimated(true);
            case UISwipeGestureRecognizerDirection.Up:
                // 上
                println("up")
            case UISwipeGestureRecognizerDirection.Down:
                // 下
                println("down")
            default:
                // その他
                println("other")
                break
            }
        }
    }
    
    func allowAuthorization() {
        println("allowAuthorized（認証許可）")
        
        // ステータスを取得
        let status: EKAuthorizationStatus = EKEventStore.authorizationStatusForEntityType(EKEntityTypeEvent)
        if status != EKAuthorizationStatus.Authorized {
            // ユーザーに許可を求める.
            myEventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: {
                (granted , error) -> Void in
                // 許可を得られなかった場合アラート発動.
                if granted {
                    return
                } else {
                    // メインスレッド 画面制御. 非同期.
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        // アラート生成
                        let myAlert = UIAlertController(title: "許可されませんでした",
                            message: "Privacy->App->Reminderで変更してください",
                            preferredStyle: UIAlertControllerStyle.Alert)
                        // アラートアクション.
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
                        myAlert.addAction(okAction)
                        self.presentViewController(myAlert, animated: true, completion: nil)
                    })
                }
            })
        }
    }// 認証許可ここまで
    
    // 開始時間
    func onDidChangeDate1(sender: UIDatePicker){
        let myDateFormatter1 = NSDateFormatter()
        myDateFormatter1.dateFormat = "yyyy/MM/dd hh:mm"
        // 日付をフォーマットに則って取得.
        let mySelectDateString1 = myDateFormatter1.stringFromDate(sender.date)
        let mySelectDate1 = myDateFormatter1.dateFromString(mySelectDateString1)!
        // Date型をここで一応、準備しておく。
        myStartDate = NSDate(timeInterval: 0, sinceDate: mySelectDate1)
        // 開始TextFieldに指定した日付を代入
        startTime.text = mySelectDateString1
    }
    
    // 終了時間
    func onDidChangeDate2(sender: UIDatePicker){
        let myDateFormatter2 = NSDateFormatter()
        myDateFormatter2.dateFormat = "yyyy/MM/dd hh:mm"
        // 日付をフォーマットに則って取得.
        let mySelectDateString2 = myDateFormatter2.stringFromDate(sender.date)
        let mySelectDate2 = myDateFormatter2.dateFromString(mySelectDateString2)!
        // Date型をここで一応、準備しておく。
        myEndDate = NSDate(timeInterval: 0, sinceDate: mySelectDate2)
        // 終了TextFieldに指定した日付を代入
        endTime!.text = mySelectDateString2
    }
    
    // DatePickerの完了ボタンが押された時
    func tappedToolBarBtn(sender: UIBarButtonItem) {
        startTime.resignFirstResponder()
        endTime.resignFirstResponder()
    }

    // iCloudカレンダー保存ボタン
    @IBAction func icloud(sender: UIButton) {
        // もし、時間指定がなければ、
        if startTime.text == "" || endTime.text == "" {
            let myAlert = UIAlertController(title: "日時を指定して下さい", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let okAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
            myAlert.addAction(okAlertAction)
            self.presentViewController(myAlert, animated: true, completion: nil)

        } else {
            // イベント作成
            let myEvent = EKEvent(eventStore: myEventStore)
            // 開始時間・終了時間からトータル秒数を算出
            let totalSec = Int(myEndDate.timeIntervalSinceDate(myStartDate))
            println("totalSec:\(totalSec)")
            
            let defaults = NSUserDefaults.standardUserDefaults()
            let mySec = defaults.integerForKey("sec")
            let taskData2 = totalSec / mySec
            println("taskData2:\(taskData2)")
            
            // イベント（TextField）の文字をmyPlanに代入
            myPlan = "タイトル：\(event.text)" + "、" + "総タスク数：\(taskData2)"
            
            // メモTextFieldの文字をmyMemoに代入
            myMemo = memo.text
            
            // イベントタイトル・開始時間・終了時間をmyEventに設定する
            myEvent.title = myPlan
            myEvent.startDate = myStartDate
            myEvent.endDate = myEndDate
            myEvent.notes = myMemo
            
            // 「一日中」の設定
            myEvent.allDay = false
            
            // イベントをカレンダーにセットする
            myEvent.calendar = myEventStore.defaultCalendarForNewEvents
            
            // イベントを保存
            let result = myEventStore.saveEvent(myEvent, span: EKSpanThisEvent, error: nil)
            
            // 保存成功・不成功のアラート
            if result {
                let myAlert = UIAlertController(title: "保存に成功しました", message: "\(myPlan)", preferredStyle: UIAlertControllerStyle.Alert)
                let okAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
                myAlert.addAction(okAlertAction)
                self.presentViewController(myAlert, animated: true, completion: nil)
                
            } else {
                let myAlert = UIAlertController(title: "保存に失敗しました", message: "\(myPlan)", preferredStyle: UIAlertControllerStyle.Alert)
                let okAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
                myAlert.addAction(okAlertAction)
                self.presentViewController(myAlert, animated: true, completion: nil)
            }
        }// もし、時間指定があれば、ここで終了
    }// iCloudカレンダー保存ボタン、ここで終了
    
    func onClickMyBarButton(sender: UIButton){
        let myTextView: TextView = TextView()
        self.navigationController?.pushViewController(myTextView, animated: true)
    }
    
    // 改行ボタンが押された際に呼ばれる。UITextFieldでReturnキーを押したときにこの関数は呼ばれる。
    // 実際に呼ばれる関数は「textFieldで最初に応答した"通知"を取り消せ。つまり、キーボードを隠せ」
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // UITextField編集直後
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        txtActiveField = textField
       return true
    }
    
    // TableView画面へ遷移
    @IBAction func OneTouch(sender: UIButton) {
        let myTableView: TableView = TableView()
        self.navigationController?.pushViewController(myTableView, animated: true)
        println("ワンタッチ入力")
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // To Be Reviewed
        // TableViewの値を代入
        let defaults = NSUserDefaults.standardUserDefaults()
        let myTableItem: String? = defaults.stringForKey("myTableItem")
        event.text = myTableItem
        
        // To Be Reviewed
        // 通知設定。viewWillAppearのタイミングなのか？
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self, selector: "handleKeyboardWillShowNotification:", name: UIKeyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: "handleKeyboardWillHideNotification:", name: UIKeyboardWillHideNotification, object: nil)
    }

    // キーボードが隠れないようにする
    func handleKeyboardWillShowNotification(notification: NSNotification) {
        let userInfo = notification.userInfo!
        
        // キーボード（最初の位置）：UIKeyboardFrameBeginUserInfoKey
        // キーボード（最後の位置）：UIKeyboardFrameEndUserInfoKey
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        var txtLimit = txtActiveField.frame.origin.y + txtActiveField.frame.height + 8.0
        let kbdLimit = myBoundSize.height - keyboardScreenEndFrame.size.height
        println("テキストフィールドの下辺：(\(txtLimit))")
        println("キーボードの上辺：(\(kbdLimit))")
        if txtLimit >= kbdLimit {
            // 差分をy方向へスクロール
            view.transform = CGAffineTransformMakeTranslation(0, -(txtLimit - kbdLimit + 10))
        }
    }

    // オフセットを戻す
    func handleKeyboardWillHideNotification(notification: NSNotification) {
        view.transform = CGAffineTransformMakeTranslation(0, 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
