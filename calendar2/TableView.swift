import UIKit
class TableView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var myTableView: UITableView = UITableView()
    var myItems = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DB().SELECT(myItems)
        myTableView.reloadData()

        self.title = "ワンタッチ入力"
        // ナビゲーションバーを取得.
        self.navigationController?.navigationBar
        // ナビゲーションバーを表示.
        self.navigationController?.navigationBarHidden = false
        // ナビゲーションバーの右側に編集ボタンを追加.
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
        // Status Barの高さを取得.
        let barHeight: CGFloat = UIApplication.sharedApplication().statusBarFrame.size.height
        // Viewの高さと幅を取得.
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        // TableViewの生成( *status barの高さ分ずらして表示 ).
        myTableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        // Cellの登録.
        myTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        // DataSourceの設定.
        myTableView.dataSource = self
        // Delegateを設定.
        myTableView.delegate = self
        // 罫線の色
        myTableView.separatorColor = UIColor.grayColor()
        // 編集中のセル選択を許可.
        myTableView.allowsSelectionDuringEditing = true
        // TableViewをViewに追加する.
        self.view.addSubview(myTableView)
    }
    
    
    // Cellが選択された際に呼び出される.
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        println("Num: \(indexPath.row)")
        println("Value: \(myItems[indexPath.row])")
        println("Edeintg: \(tableView.editing)")
        
        if myTableView.editing == true {
            println("何もしない。")
        }else{
            let myTableItem:String!
            myTableItem = myItems[indexPath.row] as! String
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(myTableItem, forKey: "myTableItem")
            self.navigationController?.popViewControllerAnimated(true);
        }
    }
    

    // Cellの総数を返す(実装必須)
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myItems.count
    }
    

    // Cellに値を設定する(実装必須)
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MyCell", forIndexPath: indexPath) as! UITableViewCell
        // Cellに値を設定.
        cell.textLabel?.text = "\(myItems[indexPath.row])"
        return cell
    }
    

    // 編集ボタンが押された際に呼び出される
    override func setEditing(editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        // TableViewを編集可能にする
        myTableView.setEditing(editing, animated: true)
        // 編集中のときのみaddButtonをナビゲーションバーの左に表示する
        if editing {
            println("編集中")
            let addButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "addCell:")
            self.navigationItem.setLeftBarButtonItem(addButton, animated: true)
        } else {
            println("通常モード")
            self.navigationItem.setLeftBarButtonItem(nil, animated: true)
        }
    }

    
    // addButtonが押された際呼び出される
    func addCell(sender: AnyObject) {
        let alert = UIAlertController(title: "新規イベント登録", message: "よく使うイベントを登録して下さい", preferredStyle: .Alert)
        var newEvent: UITextField!
        
        // To Be Reviewed
        alert.addTextFieldWithConfigurationHandler {
            newEvent = $0
        }
        
        alert.addAction(
            UIAlertAction(title: "OK", style: .Default) {
                _ in
                let myNewEvent: String = newEvent.text
                self.myItems.addObject(myNewEvent)
                self.myTableView.reloadData()
                DB().insert(myNewEvent)
            }
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        presentViewController(alert, animated: true, completion: nil)
    }
    

    // Cellを挿入または削除しようとした際に呼び出される
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // 削除のとき.
        if editingStyle == UITableViewCellEditingStyle.Delete {
            println("削除")
            let myItem: String = myItems[indexPath.row] as! String
            println(myItem)
            // 指定されたセルのオブジェクトをmyItemsから削除する.
            myItems.removeObject(myItem)
            // TableViewを再読み込み.
            myTableView.reloadData()
            DB().delete(myItem)
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
