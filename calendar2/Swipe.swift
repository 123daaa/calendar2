import UIKit
class Swipe: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        func addSwipeRecognizer() {
            var swipeLeft = UISwipeGestureRecognizer(target: self, action: "swiped:")
            swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
            
            var swipeRight = UISwipeGestureRecognizer(target: self, action: "swiped:")
            swipeRight.direction = UISwipeGestureRecognizerDirection.Right
            
            var swipeUp = UISwipeGestureRecognizer(target: self, action: "swiped:")
            swipeUp.direction = UISwipeGestureRecognizerDirection.Up
            
            var swipeDown = UISwipeGestureRecognizer(target: self, action: "swiped:")
            swipeDown.direction = UISwipeGestureRecognizerDirection.Down
            
            self.view.addGestureRecognizer(swipeLeft)
            self.view.addGestureRecognizer(swipeRight)
            self.view.addGestureRecognizer(swipeUp)
            self.view.addGestureRecognizer(swipeDown)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func swiped(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Left:
                println("left")
                
            case UISwipeGestureRecognizerDirection.Right:
                println("right")
                
            case UISwipeGestureRecognizerDirection.Up:
                println("up")
                
            case UISwipeGestureRecognizerDirection.Down:
                println("down")
                
            default:
                println("other")
                break
            }
        }
    }
}
