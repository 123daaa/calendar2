import UIKit
class DB {
    var en: String?
    var jp: String?
    var person: String?
    
    // ■■■
    func today() {
        let paths = NSSearchPathForDirectoriesInDomains(
            .DocumentDirectory,
            .UserDomainMask, true)
        let _path = paths[0].stringByAppendingPathComponent("ifuturecalendar.db")
        let db = FMDatabase(path: _path)
        let sql = "CREATE TABLE IF NOT EXISTS today (id INTEGER PRIMARY KEY AUTOINCREMENT, en TEXT, jp TEXT, person TEXT);"
        db.open()
        db.beginTransaction()
        var success = true
        let ret = db.executeUpdate(sql, withArgumentsInArray: nil)
        if ret {
            println("テーブルの作成に成功")
        }
        
        let sql2 = "INSERT INTO today (id, en, jp, person) VALUES (?, ?, ?, ?);"
        let ret2 = db.executeUpdate(sql2, withArgumentsInArray: [1, "There is always light behind the clouds.", "雲の向こうは、いつも青空。", "Louisa May Alcott（ルイーザ・メイ・オルコット）"])
        println("ret2=■\(ret2)■")
        if ret2 {
            println("■新規レコード登録■")
            let sql3 = "INSERT INTO today (id, en, jp, person) VALUES (?, ?, ?, ?);"
            
            var setTodays = [ [2, "Change before you have to.", "変革せよ。変革を迫られる前に。", "Jack Welch（ジャック・ウェルチ）"] ]
            setTodays.append([3, "If you can dream it, you can do it.", "夢見ることができれば、それは実現できる。", "Walt Disney　（ウォルト・ディズニー）"])
            setTodays.append([4, "Love the life you live. Live the life you love.", "自分の生きる人生を愛せ。自分の愛する人生を生きろ。", "Bob Marley　（ボブ・マーリー）"])
            setTodays.append([5, "My life didn’t please me, so I created my life.", "私の人生は楽しくなかった。だから私は自分の人生を創造したの。", "Coco Chanel　（ココ・シャネル）"])
            setTodays.append([6, "It always seems impossible until it’s done.", "何事も成功するまでは不可能に思えるものである。", "Nelson Mandela（ネルソン・マンデラ）"])
            setTodays.append([7, "Peace begins with a smile.", "平和は微笑みから始まります。", "Mother Teresa（マザー・テレサ）"])
            setTodays.append([8, "Love dies only when growth stops.", "愛が死ぬのは、愛の成長が止まる、その瞬間である。", "Pearl S. Buck（パール・バック）"])
            setTodays.append([9, "There is more to life than increasing its speed.", "速度を上げるばかりが、人生ではない。", "Mahatma Gandhi（ガンジー）"])
            setTodays.append([10, "Everything is practice.", "すべては練習のなかにある。", "Pele（ペレ）"])
            setTodays.append([11, "Men willingly believe what they wish.", "人は喜んで自己の望むものを信じるものだ。", "Julius Caesar（ユリウス・カエサル）"])
            setTodays.append([12, "If you want to be happy, be.", "幸せになりたいのなら、なりなさい。", "Leo Tolstoy（トルストイ）"])
            setTodays.append([13, "Without haste, but without rest.", "急がずに、だが休まずに。", "Johann Wolfgang von Goethe（ゲーテ）"])
            setTodays.append([14, "You’ll never find a rainbow if you’re looking down.", "下を向いていたら、虹を見つけることは出来ないよ。", "Charlie Chaplin（チャップリン）"])
            setTodays.append([15, "", "できれば俺のライブが終わったあと、すぐに旅にでてほしい。", "布袋寅泰"])
            setTodays.append([16, "Stay hungry. Stay foolish.", "ハングリーであれ。愚か者であれ。", "Steve Jobs （スティーブ・ジョブズ）"])
            setTodays.append([17, "Your time is limited, so don’t waste it living someone else’s life.", "あなたの時間は限られている。だから他人の人生を生きたりして無駄に過ごしてはいけない。", "Steve Jobs（スティーブ・ジョブズ）"])
            setTodays.append([18, "Fight for liberty!", "戦え！自由のために。", "Charlie Chaplin（チャップリン）"])
            setTodays.append([19, "", "未来を恐れず、過去に執着せず、今を生きろ。", "堀江貴文"])
            for setToday in setTodays {
                success = db.executeUpdate(sql3, withArgumentsInArray: setToday)
                if !success {
                    break
                }
            }// ループを抜ける
            
            if success {
                db.commit()
            } else {
                db.rollback()
            }
            
        } else {
            println("■レコード既存 = 追加なし■")
        }
        let sql4 = "SELECT * FROM today ORDER BY RANDOM() LIMIT 1;"
        let results = db.executeQuery(sql4, withArgumentsInArray: nil)
        while results.next() {
            en = results.stringForColumnIndex(1)
            jp = results.stringForColumnIndex(2)
            person = results.stringForColumnIndex(3)
        }
        db.close()
        if (results != nil) {
            println("ランダムSQL実行成功")
        }
    }
    
    
    // ■■■
    func SELECT(myItems: NSMutableArray) {
        let paths = NSSearchPathForDirectoriesInDomains(
            .DocumentDirectory,
            .UserDomainMask, true)
        let _path = paths[0].stringByAppendingPathComponent("ifuturecalendar.db")
        let db = FMDatabase(path: _path)
        let sql = "CREATE TABLE IF NOT EXISTS event (id INTEGER PRIMARY KEY AUTOINCREMENT, task TEXT);"
        db.open()
        let ret = db.executeUpdate(sql, withArgumentsInArray: nil)
        if ret {
        println("テーブルの作成に成功")
        }
        // デフォルトセルのインサート処理
        let sql2 = "INSERT INTO event (id, task) VALUES (?, ?);"
        let ret2 = db.executeUpdate(sql2, withArgumentsInArray: [1, "読書"])
        println("ret2=■\(ret2)■")
        
        let sql3 = "SELECT * FROM event ORDER BY id;"
        let results = db.executeQuery(sql3, withArgumentsInArray: nil)
        if (results != nil) {
            println("DBのレコードを取得できている。")
        }
        while results.next() {
            myItems.addObject(results.stringForColumnIndex(1))
        }
        for forMyItems in myItems {
            println(forMyItems)
        }
        db.close()
    }
    
    // ■■■
    func delete(myItem: String) {
        let paths = NSSearchPathForDirectoriesInDomains(
            .DocumentDirectory,
            .UserDomainMask, true)
        let _path = paths[0].stringByAppendingPathComponent("ifuturecalendar.db")
        let db = FMDatabase(path: _path)
        let sql = "DELETE FROM event WHERE task = ?;"
        db.open()
        db.executeUpdate(sql, withArgumentsInArray: ["\(myItem)"])
        db.close()
    }
    
    // ■■■
    func insert(myNewEvent: String) {
        let paths = NSSearchPathForDirectoriesInDomains(
            .DocumentDirectory,
            .UserDomainMask, true)
        let _path = paths[0].stringByAppendingPathComponent("ifuturecalendar.db")
        let db = FMDatabase(path: _path)
        let sql = "INSERT INTO event (task) VALUES (?);"
        db.open()
        db.executeUpdate(sql, withArgumentsInArray: ["\(myNewEvent)"])
        db.close()
    }
}
