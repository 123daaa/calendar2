import UIKit
class FirstViewController: UIViewController, UIApplicationDelegate {
    @IBOutlet weak var taskLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    @IBOutlet weak var today1: UILabel!
    @IBOutlet weak var today2: UILabel!
    @IBOutlet weak var today3: UILabel!

    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var goSecondButton: UIButton!
    
    var startTime: NSTimeInterval? = nil
    var timer: NSTimer?
    var elapsedTime: Double = 0.0
    
    var taskData: Int = 0
    var sec: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "ストップウォッチ"
        goSecondButton.layer.cornerRadius = 10
        setButtonEnabled(true, stop: false, reset: false)
        
        // UINavigationControllerのボタンに関して
            // NavigationBarを取得する.
            self.navigationController?.navigationBar
        
            // NavigationBarを表示する.
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        
            // BarButtonItemを取得する.
            self.navigationItem
        
            // BarButtonItemを作成する.
            let myBarButton = UIBarButtonItem(title: "説明", style: .Plain, target: self, action: "explanation:")
        
            // Barの右側に配置する.
            self.navigationItem.setRightBarButtonItem(myBarButton, animated: true)

        // Swipe処理
        addSwipeRecognizer()
        // Foreground通知
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "enterForeground:", name:"applicationWillEnterForeground", object: nil)
    }
    
    func enterForeground(notification: NSNotification){
        let myDB: DB = DB()
        myDB.today()
        today1.text = myDB.en
        today2.text = myDB.jp
        today3.text = myDB.person
        println("applicationWillEnterForeground")
    }
    
    override func viewWillAppear(animated: Bool) {
        let myDB: DB = DB()
        myDB.today()
        today1.text = myDB.en
        today2.text = myDB.jp
        today3.text = myDB.person
    }
    
    override func viewDidLayoutSubviews() {
        today1.numberOfLines = 0
        today1.sizeToFit()
        today2.numberOfLines = 0
        today2.sizeToFit()
    }
    
    @IBAction func goSecondButton(sender: UIButton) {
        goSecond()
    }

    func goSecond() {
        if sec == 0 {
            let myAlert = UIAlertController(title: "1秒以上、計測して下さい", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let okAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil)
            myAlert.addAction(okAlertAction)
            self.presentViewController(myAlert, animated: true, completion: nil)
            
        }else{
            setButtonEnabled(true, stop: false, reset: true)
            // ストップウォッチを止める処理
            if let t = self.startTime {
                self.elapsedTime += NSDate.timeIntervalSinceReferenceDate() - t
                self.timer?.invalidate()
                self.timer = nil
            }
            // プロパティに値を代入
            let mySecondViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("2") as! SecondViewController
            
            // To Be Reviewed
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setInteger(taskData, forKey:"taskData")
            defaults.setInteger(sec, forKey:"sec")
            
            self.navigationController?.pushViewController(mySecondViewController, animated: true)
        }
    }
    
    // スワイプ検知用に登録
    func addSwipeRecognizer() {
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        
        var swipeUp = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeUp.direction = UISwipeGestureRecognizerDirection.Up
        
        var swipeDown = UISwipeGestureRecognizer(target: self, action: "swiped:")
        swipeDown.direction = UISwipeGestureRecognizerDirection.Down
        
        self.view.addGestureRecognizer(swipeLeft)
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeUp)
        self.view.addGestureRecognizer(swipeDown)
    }
    
    // スワイプ処理
    func swiped(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.Left:
                // 左
                println("left")
                goSecond()
            case UISwipeGestureRecognizerDirection.Right:
                // 右
                println("right")
            case UISwipeGestureRecognizerDirection.Up:
                // 上
                println("up")
            case UISwipeGestureRecognizerDirection.Down:
                // 下
                println("down")
            default:
                // その他
                println("other")
                break
            }
        }
    }
    
    func explanation(sender: UIButton){
        let myTextView: TextView = TextView()
        self.navigationController?.pushViewController(myTextView, animated: true)
    }

    func setButtonEnabled(start:Bool, stop:Bool, reset:Bool) {
        self.startButton.enabled = start
        self.stopButton.enabled = stop
        self.resetButton.enabled = reset
    }
    
    func update() {
        if let t = self.startTime {
            let time: Double = NSDate.timeIntervalSinceReferenceDate() - t + self.elapsedTime
            sec = Int(time)
            let msec: Int = Int((time - Double(sec)) * 100.0)
            self.timerLabel.text = NSString(format: "%02d:%02d:%02d", sec/60, sec%60, msec) as String
            
            if sec == 0 {
                // 処理なし
            } else {
                taskData = 3600 / sec
                self.taskLabel.text = NSString(format: "%02d", taskData) as String
            }
        }
    }
    
    @IBAction func startTimer(sender: AnyObject) {
        setButtonEnabled(false, stop: true, reset: false)
        self.startTime = NSDate.timeIntervalSinceReferenceDate()// 2001/1/1 0:0:0
        self.timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: Selector("update"), userInfo: nil, repeats: true)
        
        UIView.animateWithDuration(0.3,
            animations: {() -> Void  in
                self.timerLabel.transform = CGAffineTransformMakeScale(3.0, 3.0)
                self.timerLabel.transform = CGAffineTransformMakeScale(1.0, 1.0)
                self.taskLabel.transform = CGAffineTransformMakeScale(3.0, 3.0)
                self.taskLabel.transform = CGAffineTransformMakeScale(1.0, 1.0)
        })
    }
    
    @IBAction func stopTimer(sender: AnyObject) {
        setButtonEnabled(true, stop: false, reset: true)
        if let t = self.startTime {
            self.elapsedTime += NSDate.timeIntervalSinceReferenceDate() - t
            self.timer?.invalidate()
            self.timer = nil
        }
        
        UIView.animateWithDuration(0.3,
            animations: {() -> Void  in
                self.timerLabel.transform = CGAffineTransformMakeScale(3.0, 3.0)
                self.timerLabel.transform = CGAffineTransformMakeScale(1.0, 1.0)
                self.taskLabel.transform = CGAffineTransformMakeScale(3.0, 3.0)
                self.taskLabel.transform = CGAffineTransformMakeScale(1.0, 1.0)
        })
    }
    
    @IBAction func resetTimer(sender: AnyObject) {
        setButtonEnabled(true, stop: false, reset: false)
        self.elapsedTime = 0.0
        self.startTime = nil
        self.timerLabel.text = "00:00:00"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

