import UIKit
class TextView: UIViewController {
    
    var myScrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "アプリ説明"
        self.view.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)

        let myTextView: UITextView = UITextView(frame: CGRectMake(0, 0, self.view.frame.width, self.view.frame.size.height))
        myTextView.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 1, alpha: 1.0)
        
        myTextView.text = "iFutureCalendarは、ストップウォッチ機能を使うことで、１回あたりの『作業時間』を計測します。\n\nそれによって、１時間後、２時間後など、設定した時間の中で、その作業を何回こなせるのか『作業回数』を予測できるアプリです。また、その結果を（iPhoneにデフォルト設定されている）iCloudカレンダーに保存することもできます。\n\n【操作手順】\n１、ストップウォッチで１回あたりの『作業時間』を測る。\n\n２、詳細設定ページでイベントタイトル、作業開始時間（終了時間）などを設定する。\n\n３、iCloudカレンダーに『作業回数』などを保存する。\n\n■活用例１\n「本を読む時」\n１ページ読み終えるタイムを計測することで、設定時間の中で何ページ読み進めるのか予測できる。\n\n■活用例２\n「運動時」\nランニングマシンで１km走るタイムを計測することで、数十分後に約何km走れるのか予測できる。\n\niFutureCalendarで未来を予測しよう。\n"
        // 角丸
        myTextView.layer.masksToBounds = false
        // 丸みのサイズ
        myTextView.layer.cornerRadius = 20.0
        // 枠線の太さ
        myTextView.layer.borderWidth = 1
        // 枠線の色
        myTextView.layer.borderColor = UIColor.blackColor().CGColor
        // フォント
        myTextView.font = UIFont.systemFontOfSize(CGFloat(20))
        // フォントの色
        myTextView.textColor = UIColor.blackColor()
        // 左詰め
        myTextView.textAlignment = NSTextAlignment.Left
        // テキスト編集不可
        myTextView.editable = false
        
        // To Be Reviewed
        myScrollView = UIScrollView()
        myScrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
        myScrollView.contentSize = CGSizeMake(myTextView.frame.size.width, myTextView.frame.size.height)
        myScrollView.addSubview(myTextView)
        self.view.addSubview(myScrollView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
